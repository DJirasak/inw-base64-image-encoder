# INW BASE64 IMAGE ENCODER


__Table of contents__

- [INW-BASE64-IMAGE-ENCODER](#path-logger)
  - [Installation](#installation)
  - [Usage](#usage)

## Installation

`inw-base64-image-encoder` can be installed on Linux, Mac OS or Windows without any issues.

Install via NPM

```
npm install --save inw-base64-image-encoder
```

## Usage
1. Import

```javascript
import imageEncode from 'inw-base64-image-encoder';
```

2. Encode Image

```javascript
let FileElement = document.getElementById('some-id');

FileElement.addEventListener("change", async (e) => {
    // Compress file when file size greater than 1.5 MB
    let Base64Img = await imageEncode(e.target.files[0], 1.5);
    console.log(Base64Img);
})
```

