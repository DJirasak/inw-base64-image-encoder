import imageCompression from 'browser-image-compression';
/**
 *
 * @param {File} imageFile Image file
 * @param {number} limitSize maxSizeMB=Number.POSITIVE_INFINITY (progress from 0.1)
 * @returns {Promise<File | Blob>}
 */
function readFile(data) {
    var dataRes = new Promise(function (resolve, _) {
        var FR = new FileReader();
        FR.addEventListener("load", function (e) {
            resolve(e.target.result);
        });

        FR.readAsDataURL(data);
    });
    return dataRes;
}
async function imageEncode(imageFile, limitSize = 1.0) {
    let options = {
        maxSizeMB: limitSize,
        maxWidthOrHeight: 1920,
        useWebWorker: true,
        initialQuality: 1,
    };
    var newDataUri = await new Promise(async function (resolve, _) {
        if (imageFile.size / 1024 / 1024 <= limitSize) {
            let base64IMG = await readFile(imageFile);
            resolve(base64IMG);
        } else {
            imageCompression(imageFile, options)
                .then(async function (compressedFile) {
                    let base64IMG = await readFile(compressedFile);
                    resolve(base64IMG);
                })
                .catch(function (error) {
                    console.log(error.message);
                });
        }
    });
    return newDataUri;
}

export default imageEncode;
